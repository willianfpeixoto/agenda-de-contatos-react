

const headers = {
    'Accept':'application/json',
    'Authorization': 'whatever-you-want'
}

export const listar = () =>
    fetch('/contacts', {headers})
        .then(res => res.json())
        .then(data=> data.contacts);

export const apagar = (contato) => 
    fetch(`/contacts/${contato.id}`, { method: 'DELETE', headers})
        .then(res => res.json())
        .then(data => data.contacts);

export const cadastrar = (body) =>
    fetch('/contacts', {
        method:'POST',
        headers: {
            ...headers,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(res => res.json());