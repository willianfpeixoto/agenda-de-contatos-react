import React, { Component } from 'react';
import * as ContatosAPI from './servicos/ContatosAPI';
import { Route } from 'react-router-dom';
import ListaContatos from './componentes/ListaContatos';
import CadastroContato from './componentes/CadastroContato';

export default class App extends Component {

  state = {
    screen: 'listar',
    contatos: [] 
  }

  componentDidMount() {
    ContatosAPI.listar().then((contatos)=> {
      this.setState({ contatos })
    });
  };

  apagaContato = contato => {
    this.setState( state => ({
      contatos: state.contatos.filter((c) => c.id !== contato.id)

    }))
    ContatosAPI.apagar(contato);
  };

  cadastraContato(contato) {
    ContatosAPI.cadastrar(contato).then(contato => {
      this.setState(state => ({
        contatos: state.contatos.concat([ contato ])
      }))
    })
  }
  
  render() {
    return(
      <div className="App">
        <Route exact path='/' render={() => (
          <ListaContatos
            onApagaContato={this.apagaContato}
            contatos={this.state.contatos}
            onNavigate = {() => {
              this.setState({ screen: 'cadastrar'})
            }}
          />
        )}/>

        <Route path='/cadastrar' render={({ history }) => (
          <CadastroContato
            onCadastraContato={(contato) => {
              this.cadastraContato(contato)
              history.push('/')
            }}
          />
        )}/>
      </div>
    );
  }
}
