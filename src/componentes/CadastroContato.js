import React, { Component } from 'react';
import serializeForm from 'form-serialize';
import ImageInput from '../utils/ImageInput';
import './CadastroContato.css';

export default class CadastroContato extends Component {

  handleSubmit = (e) => {
    e.preventDefault()
    const values = serializeForm(e.target, { hash: true })
    if (this.props.onCadastraContato)
        this.props.onCadastraContato(values)
  }

  render() {
    return (
    <div>
      <form onSubmit={this.handleSubmit} className='create-contact-form'>
        <ImageInput
          className='create-contact-avatar-input'
          name='avatarURL'
          maxHeight={64}
        />
        <div className='create-contact-details'>
          <input type='text' name='name' placeholder='Nome'/>
          <input type='text' name='email' placeholder='Email'/>
          <button>Adicionar contato</button>
        </div>
      </form>
    </div>
    )
  }

}