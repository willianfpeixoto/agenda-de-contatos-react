import React, {Component} from 'react';
import './ListaContatos.css';
import PropTypes from 'prop-types';
import escapeRegExp from 'escape-string-regexp';
import {Link} from 'react-router-dom';

export default class ListaContatos extends Component {

  static propTypes = {
    contatos: PropTypes.array.isRequired,
    onApagaContato: PropTypes.func.isRequired
  }

  state = {
    query: ''
  }

  updateQuery = (query) => {
    this.setState({ query: query.trim() })
  }

  clearQuery = () => {
    this.setState({ query: '' })
  }

  render() {
    const { contatos, onApagaContato } = this.props
    const { query } = this.state

    let contatosView
    if (query) {
      const match = new RegExp(escapeRegExp(query), 'i')
      contatosView = contatos.filter((contato) => match.test(contato.name))
    } else {
      contatosView = contatos
    }

    return (
      <div className='list-contacts'>
        <div className='list-contacts-top'>
          <input
            className='search-contacts'
            type='text'
            placeholder='Buscar contato'
            value={query}
            onChange={(event) => this.updateQuery(event.target.value)}
          />
          <Link to='/cadastrar' className='add-contact'>Add</Link>
        </div>

        {contatosView.length !== contatos.length && (
          <div className='showing-contacts'>
            <span>Now showing {contatosView.length} of {contatos.length} total</span>
            <button onClick={this.clearQuery}>Show all</button>
          </div>
        )}

        <ol className='contact-list'>
          {contatosView.map((contato) => (
            <li key={contato.id} className='contact-list-item'>
              <div className='contact-avatar' style={{ backgroundImage: `url(${contato.avatarURL})`}}/>
              <div className='contact-details'> <p>{contato.name}</p></div>
              <div className='contact-details'><p>{contato.email}</p></div>
              <button onClick={() => onApagaContato(contato)} className='contact-remove'/>
            </li>
          ))}
        </ol>
      </div>
    )
  }
}